\iffalse
\textbf{Terna destrorsa di versori} (\emph{notazione di Einstein})
\[ \hat x_i \cdot \hat x_j = \delta_{ij} \]
\[ \hat x_i \wedge \hat x_j = \varepsilon_{ijk} \hat x_k \]
\fi

\textbf{Identità}
\[ \varepsilon_{ijk} \varepsilon_{ilm} = \delta_{jl}\delta_{km} - \delta_{jm}\delta_{kl} \]
\[ \varepsilon_{ijk} \varepsilon_{ijm} = 2\delta_{km} \]
\[ \vec a \cdot (\vec b \wedge \vec c) = \vec b \cdot (\vec c \wedge \vec a ) = \vec c \cdot (\vec a \wedge \vec b) \]
\[ \vec a \wedge (\vec b \wedge \vec c) = \vec b (\vec a \cdot \vec c) - \vec c (\vec a \cdot \vec b) \]
\[ \vec a \wedge (\vec b \wedge \vec c) + \vec c \wedge (\vec a \wedge \vec b) + \vec b \wedge (\vec c \wedge \vec a) = 0 \]
\[ \left( \vec a \wedge \vec b \right)^2 = a^2 b^2 - (\vec a \cdot \vec b)^2 \]

\textbf{Rotazione di un vettore}
\begin{align*}
\vec{v}\, ' = \left( \vec v \cdot \hat n \right) \hat n + \cos\alpha \left( \vec v - \left( \vec v \cdot \hat n \right) \hat n  \right) + \sin\alpha \left( \hat n \wedge \vec v \right)
\end{align*}
\iffalse
Rotazione infinitesima (commutativa):
\begin{align*}
\vec{v}\, ' = \vec v + 	\delta\alpha \left(\hat n \wedge \vec v \right)
\end{align*}
Matrice di rotazione:
\[ R=(nn^T)(1-\cos\theta)+I\cos\theta+(\sin{\theta})[n]_{\times} \]
con $n$ vettore colonna unitario, $\theta$ angolo di rotazione, $I$ matrice identità e:
\[ [n]_{\times} = \left(\begin{smallmatrix} 0 & -n_z & n_y \\ n_z & 0 & -n_x \\ -n_y & n_x & 0\end{smallmatrix}\right)\]
\fi

\iffalse
\textbf{Derivata di un versore}
\begin{align*}
\dot{\hat x} = \vec{\Omega} \wedge \hat x
\end{align*}
\fi

\textbf{Vettore in sistema rotante}
\begin{align*}
\vec r &= x_i \hat x_i\\
\dot{\vec r} &= \dot x_i \hat x_i + (\vec\Omega \wedge x_i\hat x_i) = \vec v_r + \vec\Omega \wedge \vec r\\
\ddot{\vec r} &= \ddot x_i \hat x_i + 2\, \vec\Omega \wedge \dot x_i \hat x_i + \vec\Omega \wedge \left(\vec\Omega \wedge x_i \hat x_i\right) + \dot{\vec\Omega} \wedge x_i \hat x_i\\
&= \vec a_r + 2\, \vec{\Omega} \wedge \vec v_r + \vec\Omega \wedge (\vec\Omega \wedge \vec r) + \dot{\vec\Omega} \wedge \vec r
\end{align*}

\textbf{Sistema non inerziale}
\[ \ddot{\vec r}_I = \ddot{\vec R} + \ddot{\vec r} \]
$ \ddot{\vec r}_I $ è l'accelerazione misurata nel sistema \textit{inerziale}, $ \ddot{\vec r} $ quella nel sistema \textit{non inerziale} (vedi paragrafo precedente). $ \vec R $ è il raggio vettore dell'origine del sistema \textit{non inerziale} rispetto al sistema \textit{inerziale}. $ \vec F = m\ddot{\vec r}_I $ e quindi:
\[ m\vec a_r  = \vec F -m\ddot{\vec R} - 2m\,\vec{\Omega} \wedge \vec v_r - m\,\vec\Omega \wedge (\vec\Omega \wedge \vec r) - m\,\dot{\vec\Omega} \wedge \vec r \]
Potenziale centrifugo:
\[ V_c = -\frac12 m \left(\vec\Omega \wedge \vec r\right)^2 \]

\textbf{Coordinate polari}
\begin{align*}
\vec{r} &= r \hat r\\
\dot{\vec r} &= \dot r\ \hat r + r\dot\phi\ \hat\phi\\
\ddot{\vec r} &= \left(\ddot r - r\dot\phi^2\right)\hat r + \left(2\dot r \dot\phi + r\ddot\phi\right)\hat\phi\\
\end{align*}
\iffalse
Dette $ (x, y) $ le coordinate di un punto nel piano definiamo
\[\operatorname{arctan2}(x, y) =
\begin{cases}
\arctan(\frac{y}{x}) & \mbox{se } x > 0\\
\arctan(\frac{y}{x}) + \pi & \mbox{se } x < 0 \mbox{ e } y \ge 0\\
\arctan(\frac{y}{x}) - \pi & \mbox{se } x < 0 \mbox{ e } y < 0\\
\frac{\pi}{2} & \mbox{se } x = 0 \mbox{ e } y > 0\\
-\frac{\pi}{2} & \mbox{se } x = 0 \mbox{ e } y < 0\\
\text{non definito} & \mbox{se } x = 0 \mbox{ e } y = 0
\end{cases}
\]
\fi

\textbf{Coordinate sferiche}\\
\textit{Nota}: $\theta$ è la colatitudine, $\phi$ è l'azimuth.
\begin{align*}
\vec{r} &= r \hat r\\
\dot{\vec r} &= \dot r\ \hat r + r \dot\theta\ \hat\theta + r\dot\phi\sin\theta\ \hat\phi\\
\ddot{\vec r} &= \left(\ddot r - r\dot\theta^2 - r\dot\phi^2\sin^2\theta\right)\hat r\\
\nonumber	&+ \left(2\dot r \dot\theta + r\ddot\theta - r\dot\phi^2\sin\theta\cos\theta\right)\hat\theta\\
\nonumber	&+ \left(2\dot r \dot\phi\sin\theta + 2r\dot\theta\dot\phi\cos\theta + r\ddot\phi\sin\theta\right)\hat\phi
\end{align*}

\textbf{Aree e volumi infinitesimi}
\begin{align*}
\mbox{(Polari) } \dif{A} &= r \dif{r} \dif{\phi} \\
\mbox{(Cilindriche) } \dif{V} &= r \dif{r} \dif{\phi} \dif{z} \\
\mbox{(Sferiche) } \dif{V} &= r^2 \sin\theta \dif{r} \dif{\theta} \dif{\phi}
\end{align*}

\textbf{Coordinate intrinseche}
\begin{align*}
    \vec v &= v \hat\tau \\
    \vec a &= \dot v \hat\tau + v \dot\psi \hat n = \dot v \hat\tau + (v^2/R) \hat n 
\end{align*}
Raggio di curvatura:
\[ \frac{1}{R} = \frac{v_x \dot v_y - \dot v_x v_y}{v^3} = \frac{y''}{\left( 1+ y'^2\right)^{3/2}} \]