Questo è un formulario per il corso di Complementi di Meccanica Classica del primo anno in SNS per matematici e fisici. Può essere utile portarlo agli scritti selezionando di volta in volta gli argomenti oggetto della prova (in modo che stiano in un unico foglio A4).

[Link al PDF completo](https://gitlab.com/marco-venuti/formulario---complementi-di-fisica/-/jobs/artifacts/master/raw/formulario.pdf?job=compile_pdf)